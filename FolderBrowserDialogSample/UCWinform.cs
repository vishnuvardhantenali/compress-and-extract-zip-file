﻿using System;
using System.Windows.Forms;

namespace FolderBrowserDialogSample
{
    public partial class UCWinform : UserControl
    {
        public UCWinform()
        {
            InitializeComponent();

            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            
            // Show the FolderBrowserDialog.  
            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                BrowserPath = folderDlg.SelectedPath;
                Environment.SpecialFolder root = folderDlg.RootFolder;
            }
        }

        public string BrowserPath { get; private set; }
    }
}
