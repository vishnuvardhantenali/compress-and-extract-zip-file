﻿using Microsoft.Win32;
using System.Activities;
using System.IO;
using System.IO.Compression;
using System.Windows;
using System.Windows.Controls;

namespace FolderBrowserDialogSample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public string zipDestination { get; set; }
        public string zipSource { get; set; }
        public string dirComp { get; set; }
        public string cmpDestination { get; set; }
        public string cmpSource { get; set; }
        public int NoOfFolder { get; set; } = 50;

        /// <summary>
        /// browsing the zip files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnZipBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Title = "Browse Zip File";
            openFileDialog1.Filter = "Zip files (*.zip)|*.zip";
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == true)
            {
                zipSource = openFileDialog1.FileName;
                zipDestination = Path.ChangeExtension(zipSource, null);
                txtZipDir.Text = zipDestination;
            }
        }

        /// <summary>
        /// extracting the zip file into normal file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnZipExtract_Click(object sender, RoutedEventArgs e)
        {
            if (!string.Equals(txtZipDir.Text, zipSource.Split('.')[0]))
            {
                zipSource = Path.GetFullPath(txtZipDir.Text.Split('.')[0]);
            }

            string folderName = zipSource.Split('.')[0];

            if (!File.Exists(folderName) && Directory.Exists(folderName) == false) 
            {
                ZipFile.ExtractToDirectory(zipSource, zipDestination);

                if (ChkExtractStatus.IsChecked == true)
                    System.Diagnostics.Process.Start(zipDestination);
                return;
            }

            if (Directory.Exists(folderName))
            {
                for (int x = 1; x <= NoOfFolder; x++)
                {
                    string tempFolder = folderName.Split('.')[0] + "(" + x.ToString().PadLeft(1, '0') + ")";
                    if (!Directory.Exists(tempFolder))
                    {
                        zipDestination = tempFolder;

                        ZipFile.ExtractToDirectory(zipSource, zipDestination);

                        if (ChkExtractStatus.IsChecked == true)
                            System.Diagnostics.Process.Start(zipDestination);
                        break;
                    }
                    else if (x == NoOfFolder)
                    {
                        MessageBox.Show($"Maximum copied files are reached {NoOfFolder}");
                    }
                }
            }
        }

        /// <summary>
        /// close the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnZipClose_Click(object sender, RoutedEventArgs e)
        {           
            Close();
            //NewFolder();
        }

        /// <summary>
        /// browsing the compressed file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCompBrowse_Click(object sender, RoutedEventArgs e)
        {
            UCWinform uCWinform = new UCWinform();
            cmpSource = uCWinform.BrowserPath;
            if (!string.IsNullOrEmpty(cmpSource))
            {
                cmpDestination = cmpSource + ".zip";
                txtCompDir.Text = cmpDestination;
            }
        }

        /// <summary>
        /// conversting the folder to compressed zip file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCompress_Click(object sender, RoutedEventArgs e)
        {
            if (!string.Equals(txtCompDir.Text.Split('.')[0], cmpSource)) 
            {
                cmpSource = Path.GetFullPath(txtCompDir.Text.Split('.')[0]);
            }
            
            string folderName = Path.Combine(cmpSource);

            if (!Directory.Exists(folderName))
            {
                Directory.CreateDirectory(folderName);
                return;
            }
            else
            {
                for (int x = 1; x <= NoOfFolder; x++)
                {
                    string tempFolder = folderName + "(" + x.ToString().PadLeft(1, '0') + ").zip";
                    if (!File.Exists(tempFolder))
                    {
                        cmpDestination = tempFolder;

                        ZipFile.CreateFromDirectory(cmpSource, cmpDestination);

                        if (ChkCompressStatus.IsChecked == true)
                            System.Diagnostics.Process.Start(cmpDestination);
                        break;
                    }
                }                
            }
        }
        
        //Creates a copied Folder with indexing values.
        public void NewFolder()
        {
            try
            {
                string folderName = Path.Combine(cmpSource);
                
                if (!Directory.Exists(folderName))
                {
                    Directory.CreateDirectory(folderName);                    
                }
                else
                {
                    for (int x = 1; x <= NoOfFolder; x++)
                    {
                        string tempFolder = folderName + "(" + x.ToString().PadLeft(1, '0') + ")";
                        if (!Directory.Exists(tempFolder))
                        {
                            Directory.CreateDirectory(tempFolder);
                            
                            CopyFilesRecursively(folderName, tempFolder);
                            break;
                        }
                    }
                    MessageBox.Show($"Maximum {NoOfFolder} of Copies only created");
                }
            }
            catch
            {

            } 
        }

        /// <summary>
        /// creating the copied folders with content
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="targetPath"></param>
        private static void CopyFilesRecursively(string sourcePath, string targetPath)
        {
            try
            {
                //Now Create all of the directories
                foreach (string dirPath in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
                {
                    Directory.CreateDirectory(dirPath.Replace(sourcePath, targetPath));
                }

                //Copy all the files & Replaces any files with the same name
                foreach (string newPath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
                {
                    File.Copy(newPath, newPath.Replace(sourcePath, targetPath), true);
                }
            }
            catch
            {

            }
        }
    }
}
